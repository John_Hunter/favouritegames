﻿using FavouriteGames.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FavouriteGames.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string id)
        {
            GameLibrary library = new GameLibrary();

            GameDetails viewModel = null;
            if(!string.IsNullOrEmpty(id))
            {
                var gameId = int.Parse(id);
                viewModel = library.GetGameById(gameId);
            }
            else
            {
                viewModel = new GameDetails();
            }
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(GameDetails game)
        {
            var library = new GameLibrary();
            library.AddGame(game);
            return new RedirectResult("~/Home/List");
        }

        public ActionResult List()
        {
            var library = new GameLibrary();
            var games = library.GetAllGames();
            return View(games);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}