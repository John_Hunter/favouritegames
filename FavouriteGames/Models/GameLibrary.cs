﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FavouriteGames.Models
{
    public class GameLibrary
    {
        public void AddGame(GameDetails game)
        {
            List<GameDetails> gameList = GetAllGames();
            game.Id = gameList.Count + 1;
            gameList.Add(game);
            HttpContext.Current.Cache.Add("gameList", gameList, null, DateTime.Now.AddMonths(1), TimeSpan.FromSeconds(0), System.Web.Caching.CacheItemPriority.Low, null);
        }

        internal List<GameDetails> GetAllGames()
        {
            List<GameDetails> gameList = null;
            if (HttpContext.Current.Cache["gameList"] != null)
            {
                gameList = (List<GameDetails>)HttpContext.Current.Cache["gameList"];
            }
            else
            {
                gameList = new List<GameDetails>();
            }
            return gameList;
        }

        internal GameDetails GetGameById(int gameId)
        {
            var gameList = GetAllGames();
            var selectedGame = gameList.FirstOrDefault(game => game.Id == gameId);
            return selectedGame;
        }
    }
}
